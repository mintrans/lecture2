package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

//тесты
public class MyBranchingTest {

    @Test
    public void maxIntTest() {

        /**
         * Проверка MyBranching#maxInt(i1, i2)
         * @param i1 - первое значение
         * @param i2 - второе значение
         * @return - большее из значений или 0, если Utils#utilFunc2 вернёт true
         */
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);
        /**
         * проверка при Utils#utilFunc2() == true, @return ожидаем = 0
         */
        Assertions.assertEquals(0, myBranching.maxInt(10, 20));

        /**
         * проверка при Utils#utilFunc2() == false
         */
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        /**
         * проверка при i1 < i2, @return ожидаем = i2
         */
        Assertions.assertEquals(20, myBranching.maxInt(10, 20));

        /**
         * проверка при i1 > i2, @return ожидаем = i1
         */
        Assertions.assertEquals(15, myBranching.maxInt(15, 5));

        /**
         * проверка при i1 = i2, @return ожидаем = i1
         */
        Assertions.assertEquals(7, myBranching.maxInt(7, 7));
    }

    @Test
    public void ifElseExampleTest() {

        /**
         * Проверка MyBranching#ifElseExample()
         * @return - true, если Utils#utilFunc2() возвращает true, иначе - false
         */
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        /**
         * Utils#utilFunc2() == true, @return ожидаем = true
         */
        Assertions.assertEquals(true, myBranching.ifElseExample());

        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        /**
         * Utils#utilFunc2() == true, @return ожидаем = false
         */
        Assertions.assertEquals(false, myBranching.ifElseExample());
    }

    @Test
    public void switchExampleTest01() {

        /**
         * Проверка MyBranching#switchExample(i)
         * @param i - входящее значение (может быть любым)
         */
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        /**
         * @param i == 0
         * Utils#utilFunc1() == true
         * Utils#utilFunc2() == true
         * ожидаемое количество вызовов Utils#utilFunc1() = 1
         * ожидаемое количество вызовов Utils#utilFunc2() = 1
         */
        myBranching.switchExample(0);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    @Test
    public void switchExampleTest02() {

        /**
         * @param i == 0
         * Utils#utilFunc1() == true
         * Utils#utilFunc2() == false
         * ожидаемое количество вызовов Utils#utilFunc1() = 0
         * ожидаемое количество вызовов Utils#utilFunc2() = 1
         */
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.switchExample(0);
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    @Test
    public void switchExampleTest1() {

        /**
         * @param i == 1
         * Utils#utilFunc1() == true
         * Utils#utilFunc2() == true
         * ожидаемое количество вызовов Utils#utilFunc1() = 1
         * ожидаемое количество вызовов Utils#utilFunc2() = 1
         */
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.switchExample(1);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }

    @Test
    public void switchExampleTest2() {

        /**
         * @param i == 2
         * Utils#utilFunc1() == true
         * Utils#utilFunc2() == true
         * ожидаемое количество вызовов Utils#utilFunc1() = 0
         * ожидаемое количество вызовов Utils#utilFunc2() = 1
         */
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(true);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.switchExample(2);
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
    }
}
